Tax Calculator
================

Tax Calculator is a simple application that prints out the details of a receipt containing items purchased by a customer

The receipt should list 

* the quantity of the item
* the name of the item
* the final price (including taxes) of each of the item purchased 
* the total amount of the receipt
* the total amount of taxes

Taxes are calculated with a rate of 17.5%, rounding the tax result to the upper 0.05. 

* Medical products are exempt
* an additional 1.25 fixed amount is added as an extra tax on CDs
 
The solution proposed is based on:

* defining a parent abstract class for the product. It is responsibility of each specific product to
implement a calculateFinalPrice and calculateTaxes methods.
* defining a Tax interface and several implementations for it in order to define several ways to calculate taxes
and using aggregation to use the specific tax implementation in the specific product.
* implementing a Receipt class that has a list of products as attribute and defines a getTotal and getTaxes
to retrieve the values to be printed in the receipt.
* implementing a ReceiptFormatter to separate the display of the receipt from the actual calculation of the values.
At the moment only one method is implemented, printToConsole but if the requirements change in the future to have the 
receipt in Json format a new method getJsonReceipt could be added.
* a main class is implemented to run the application on two simple examples.

All the code has been implemented using Java and tested using Junit 4.
