import com.taxcalculator.products.GenericProduct;
import com.taxcalculator.products.MediaProduct;
import com.taxcalculator.products.MedicalProduct;
import com.taxcalculator.products.Product;
import com.taxcalculator.tax.GenericTax;
import com.taxcalculator.tax.MediaTax;
import com.taxcalculator.tax.Tax;
import org.junit.Test;

import static org.junit.Assert.*;

public class ProductTest {

    @Test
    public void shouldReturnCorrectPriceForGenericProducts() {
        Tax tax = new GenericTax();
        Product genericProduct = new GenericProduct(1,"book", 29.49, tax);
        double price = genericProduct.calculateFinalPrice();
        assertEquals(34.69, price, 0);
    }

    @Test
    public void shouldReturnCorrectPriceForCdProducts() {
        Tax tax = new MediaTax();
        Product cdProducts = new MediaProduct(1,"music CD", 15.99, tax);
        double price = cdProducts.calculateFinalPrice();
        assertEquals(20.04, price, 0);
    }

    @Test
    public void shouldReturnCorrectPriceForMedicalProducts() {
        Product medicalProduct = new MedicalProduct(1,"box of tooth ache pills", 4.15);
        double price = medicalProduct.calculateFinalPrice();
        assertEquals(4.15, price, 0);
    }

}