import com.taxcalculator.Receipt;
import com.taxcalculator.products.GenericProduct;
import com.taxcalculator.products.MediaProduct;
import com.taxcalculator.products.Product;
import com.taxcalculator.tax.GenericTax;
import com.taxcalculator.tax.MediaTax;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class ReceiptTest {

    private List<Product> products;

    @Before
    public void setUp() {
        products = new ArrayList<>();

        Product book = new GenericProduct(1, "book", 29.49, new GenericTax());
        Product cd = new MediaProduct(1, "music cd", 15.99, new MediaTax());
        Product snack = new GenericProduct(1, "chocolate snack", 0.75, new GenericTax());

        products.add(book);
        products.add(cd);
        products.add(snack);
    }

    @Test
    public void shouldCalculateTheCorrectTotalOnTheReceipt() {

        Receipt receipt = new Receipt(products);

        double total = receipt.getTotal();

        assertEquals(55.629999999999995, total, 0);

    }

    @Test
    public void shouldCalculateTheCorrectTaxesOnTheReceipt() {

        Receipt receipt = new Receipt(products);

        double receiptTaxes = receipt.getTaxes();

        assertEquals(9.4, receiptTaxes, 0);

    }

}