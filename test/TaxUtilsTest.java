import com.taxcalculator.tax.TaxUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class TaxUtilsTest {

    @Parameterized.Parameters(name = "{index}: round({0}) to {1}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][] {
                { 0.13125, 0.15 }, { 5.160749999999999, 5.20 }, { 3.67325, 3.70 }, { 3.6732499999999995, 3.70 }
        });
    }

    private double inputTax;
    private double expectedTax;

    public TaxUtilsTest(double inputTax, double expectedTax) {
        this.inputTax = inputTax;
        this.expectedTax = expectedTax;
    }

    @Test
    public void test() {
        assertEquals(expectedTax, TaxUtils.roundTaxes(inputTax), 0);
    }

}