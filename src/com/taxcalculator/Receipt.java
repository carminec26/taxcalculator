package com.taxcalculator;

import com.taxcalculator.products.Product;

import java.util.List;

public class Receipt {

    private List<Product> products;

    public Receipt(List<Product> products) {
        this.products = products;
    }

    public double getTotal() {
        return products.stream().mapToDouble(p -> p.calculateFinalPrice()).sum();
    }

    public double getTaxes() {
        return products.stream().mapToDouble(p -> p.calculateTaxes()).sum();
    }

    public List<Product> getProducts() {
        return products;
    }
}
