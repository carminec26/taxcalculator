package com.taxcalculator.tax;

public class MediaTax implements Tax {

    double TAX_RATE = 17.5;
    double FIX_TAX_RATE = 1.25;

    @Override
    public double calculateTaxes(double price) {
        double taxes = (TAX_RATE/100) * price;
        return TaxUtils.roundTaxes(taxes + FIX_TAX_RATE);
    }

}
