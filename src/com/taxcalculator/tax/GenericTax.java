package com.taxcalculator.tax;

public class GenericTax implements Tax {

    double TAX_RATE = 17.5;

    @Override
    public double calculateTaxes(double price) {
        double taxes = (TAX_RATE/100) * price;
        return TaxUtils.roundTaxes(taxes);
    }

}
