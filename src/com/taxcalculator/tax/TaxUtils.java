package com.taxcalculator.tax;

public class TaxUtils {

    static final double ROUND_FACTOR = 20;

    public static double roundTaxes(double tax) {
        return Math.ceil(tax * ROUND_FACTOR) / ROUND_FACTOR;
    }

}
