package com.taxcalculator.tax;

public interface Tax {

    double calculateTaxes(double price);

}
