package com.taxcalculator;

import com.taxcalculator.products.GenericProduct;
import com.taxcalculator.products.MediaProduct;
import com.taxcalculator.products.MedicalProduct;
import com.taxcalculator.products.Product;
import com.taxcalculator.tax.GenericTax;
import com.taxcalculator.tax.MediaTax;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        List<Product> products = new ArrayList<>();
        ReceiptFormatter receiptFormatter = new ReceiptFormatter();

        Product book = new GenericProduct(1, "book", 29.49, new GenericTax());
        Product cd = new MediaProduct(1, "music cd", 15.99, new MediaTax());
        Product snack = new GenericProduct(1, "chocolate snack", 0.75, new GenericTax());

        products.add(book);
        products.add(cd);
        products.add(snack);

        receiptFormatter.setReceipt(new Receipt(products));
        receiptFormatter.printToConsole();

        products.clear();
        Product wine = new GenericProduct(1, "bottle of wine", 20.99, new GenericTax());
        Product pills = new MedicalProduct(1, "pills", 4.15);
        Product pins = new GenericProduct(1, "box of pins", 11.25, new GenericTax());
        Product anotherCd = new MediaProduct(1, "music cd", 14.99, new MediaTax());

        products.add(wine);
        products.add(pills);
        products.add(pins);
        products.add(anotherCd);

        receiptFormatter.setReceipt(new Receipt(products));
        receiptFormatter.printToConsole();

    }
}
