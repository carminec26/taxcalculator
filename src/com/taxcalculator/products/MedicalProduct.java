package com.taxcalculator.products;

public class MedicalProduct extends Product {

    public MedicalProduct(int quantity, String name, double price) {
        super(quantity, name, price);
    }

    @Override
    public double calculateFinalPrice() {
        return getPrice();
    }

    @Override
    public double calculateTaxes() {
        return 0;
    }

}
