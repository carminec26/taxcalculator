package com.taxcalculator.products;

import com.taxcalculator.tax.Tax;

public class GenericProduct extends Product {

    private Tax tax;

    public GenericProduct(int quantity, String name, double price, Tax tax) {
        super(quantity, name, price);
        this.tax = tax;
    }

    @Override
    public double calculateFinalPrice() {
        return getPrice() + tax.calculateTaxes(getPrice());
    }

    @Override
    public double calculateTaxes() {
        return tax.calculateTaxes(getPrice());
    }

}
