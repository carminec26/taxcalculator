package com.taxcalculator.products;

public abstract class Product {

    private int quantity;
    private String name;
    private double price;

    public Product(int quantity, String name, double price) {
        this.quantity = quantity;
        this.name = name;
        this.price = price;
    }

    public abstract double calculateFinalPrice();
    public abstract double calculateTaxes();

    public int getQuantity() {
        return quantity;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }
}
