package com.taxcalculator;

import java.text.DecimalFormat;

public class ReceiptFormatter {

    private Receipt receipt;

    public void printToConsole() {
        receipt.getProducts().stream().forEach(
                p -> System.out.println(p.getQuantity() + " " + p.getName() + " " + formatToTwoDecimalPlaces(p.calculateFinalPrice())));

        System.out.println("Sales Taxes: " + formatToTwoDecimalPlaces(receipt.getTaxes()));
        System.out.println("Total: " + formatToTwoDecimalPlaces(receipt.getTotal()));
        System.out.println(System.lineSeparator());
    }

    public Receipt getReceipt() {
        return receipt;
    }

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    private String formatToTwoDecimalPlaces(double value) {
        DecimalFormat df = new DecimalFormat("#.##");
        return df.format(value);
    }

}
